package com.jjshop.dao;

import com.jjshop.model.CustomerOrder;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by jin on 2016-06-29.
 */

@Repository
@Transactional
public class CustomerOrderDaoImpl implements CustomerOrderDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addCustomerOrder(CustomerOrder cusOrder) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(cusOrder);
        session.flush();
    }

}
