package com.jjshop.dao;

import com.jjshop.model.CustomerOrder;

/**
 * Created by jin on 2016-06-29.
 */
public interface CustomerOrderDao {

    void addCustomerOrder(CustomerOrder cusOrder);

}
