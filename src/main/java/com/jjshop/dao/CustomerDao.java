package com.jjshop.dao;

import com.jjshop.model.Customer;

import java.util.List;

/**
 * Created by jin on 2016-06-27.
 */
public interface CustomerDao {

    void addCustomer(Customer customer);

    Customer getCustomerById(int customerId);

    List<Customer> getAllCustomer();

    Customer getCustomerByUsername (String username);

}
