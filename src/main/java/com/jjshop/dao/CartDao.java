package com.jjshop.dao;

import com.jjshop.model.Cart;

import java.io.IOException;

/**
 * Created by jin on 2016-06-27.
 */
public interface CartDao {

    Cart getCartById(int cartId);

    Cart validate(int cartId) throws IOException;

    void update(Cart cart);

}
