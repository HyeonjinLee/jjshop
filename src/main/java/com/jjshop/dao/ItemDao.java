package com.jjshop.dao;

import com.jjshop.model.Item;

import java.util.List;

/**
 * Created by jin on 2016-06-22.
 */


public interface ItemDao {

    void addItem(Item item);

    void editItem(Item item);

    Item getItemById(int itemId);

    List<Item> getAllItems();

    void deleteItem(Item item);

}
