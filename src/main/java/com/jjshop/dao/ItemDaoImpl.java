package com.jjshop.dao;

import com.jjshop.model.Item;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by jin on 2016-06-22.
 */

@Repository
@Transactional
public class ItemDaoImpl implements ItemDao {

    @Autowired
    private SessionFactory sessionFactory;


    public Item getItemById(int itemId) {
        Session session = sessionFactory.getCurrentSession();
        Item item = (Item) session.get(Item.class, itemId);
        session.flush();

        return item;
    }

    public List<Item> getAllItems() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Item");
        List<Item> items  = query.list();
        session.flush();

        return items;
    }

    public void addItem(Item item) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(item);
        session.flush();
    }

    public void editItem(Item item) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(item);
        session.flush();
    }

    public void deleteItem(Item item) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(item);
        session.flush();
    }

}
