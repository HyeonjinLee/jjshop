//package com.jjshop.configuration;
//
//import com.jjshop.controller.HomeController;
//import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
//
//import javax.servlet.Filter;
//
///**
// * Created by jin on 2016-06-27.
// */
//public class jjshopInitializer extends AbstractAnnotationConfigDispatcherServletInitializer{
//    @Override
//    protected Class<?>[] getRootConfigClasses() {
//        return new Class[] { HomeController.class };
//    }
//
//    @Override
//    protected Class<?>[] getServletConfigClasses() {
//        return null;
//    }
//
//    @Override
//    protected String[] getServletMappings() {
//        return new String[] { "/" };
//    }
//
//    @Override
//    protected Filter[] getServletFilters() {
//        Filter [] singleton = { new CORSFilter() };
//        return singleton;
//    }
//}
