package com.jjshop.controller;

import com.jjshop.model.Item;
import com.jjshop.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.util.List;

/**
 * Created by jin on 2016-06-27.
 */

@Controller
@RequestMapping("/item")
public class ItemController {

    @Autowired
    private ItemService itemService;

    @RequestMapping("/itemList")
    public String getItems(Model model) {
        List<Item> items = itemService.getItemList();
        model.addAttribute("items", items);

        return "itemList";
    }

    @RequestMapping("/detailItem/{itemId}")
    public String detailItem(@PathVariable(value = "itemId") int itemId, Model model) throws IOException {
        Item item = itemService.getItemById(itemId);
        model.addAttribute("item", item);

        return "detailItem";
    }
}
