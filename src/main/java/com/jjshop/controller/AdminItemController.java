package com.jjshop.controller;

import com.jjshop.model.Item;
import com.jjshop.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by jin on 2016-06-27.
 */

@Controller
@RequestMapping("/admin")
public class AdminItemController {

    private Path path;

    @Autowired
    private ItemService itemService;

    @RequestMapping("/item/addItem")
    public String addItem(Model model) {
        Item item = new Item();
        item.setItemCategory("cosmetic");
        item.setItemCondition("new");
        item.setItemStatus("active");

        model.addAttribute("item", item);

        return "addItem";
    }

    @RequestMapping(value = "/item/addItem", method = RequestMethod.POST)
    public String addItemPost(@Valid @ModelAttribute("item") Item item, BindingResult result,
                             HttpServletRequest request) {

        if (result.hasErrors()) {
            return "addItem";
        }

        itemService.addItem(item);

        MultipartFile itemImage = item.getItemImage();
        String rootDirectory = request.getSession().getServletContext().getRealPath("/");
        path = Paths.get(rootDirectory + "/WEB-INF/resources/images/" + item.getItemId() + ".jpg");

        if (itemImage != null && !itemImage.isEmpty()) {
            try {
                itemImage.transferTo(new File(path.toString()));
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("Item image saving failed!!!!", e);
            }
        }

        return "redirect:/admin/itemInventory";
    }


    @RequestMapping("/item/editItem/{itemId}")
    public String editItem(@PathVariable("itemId") int itemId, Model model) {
        Item item = itemService.getItemById(itemId);

        model.addAttribute("item", item);

        return "editItem";
    }

    @RequestMapping(value = "/item/editItem", method = RequestMethod.POST)
    public String editITemPost(@Valid @ModelAttribute("item") Item item, BindingResult result,
                             HttpServletRequest request) {

        if (result.hasErrors()) {
            return "editItem";
        }

        MultipartFile itemImage = item.getItemImage();
        String rootDirectory = request.getSession().getServletContext().getRealPath("/");
        path = Paths.get(rootDirectory + "\\WEB-INF\\resources\\images\\" + item.getItemId() + ".jpg");

        if (itemImage != null && !itemImage.isEmpty()) {
            try {
                itemImage.transferTo(new File(path.toString()));
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("Item image saving failed!!!!", e);
            }
        }

        itemService.editItem(item);

        return "redirect:/admin/itemInventory";
    }

    @RequestMapping("/item/deleteItem/{itemId}")
    public String deleteItem (@PathVariable int itemId, Model model, HttpServletRequest request) {
        String rootDirectory = request.getSession().getServletContext().getRealPath("/");
        path = Paths.get(rootDirectory + "\\WEB-INF\\resources\\images\\" + itemId + ".jpg");

        if(Files.exists(path)) {
            try {
                Files.delete(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Item item = itemService.getItemById(itemId);
        itemService.deleteItem(item);

        return "redirect:/admin/itemInventory";
    }

}
