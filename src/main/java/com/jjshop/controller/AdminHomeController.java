package com.jjshop.controller;

import com.jjshop.model.Customer;
import com.jjshop.model.Item;
import com.jjshop.service.CustomerService;
import com.jjshop.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by jin on 2016-06-27.
 */

@Controller
@RequestMapping("/admin")
public class AdminHomeController {

    @Autowired
    private ItemService itemService;

    @Autowired
    private CustomerService customerService;

    @RequestMapping
    public String adminMain(){
        return "admin";
    }

    @RequestMapping("/itemInventory")
    public String itemInventory(Model model) {
        List<Item> items = itemService.getItemList();
        model.addAttribute("items", items);

        return "itemInventory";
    }

    @RequestMapping("/customer")
    public String customerPage(Model model) {

        List<Customer> customerList = customerService.getAllCustomer();
        model.addAttribute("customerList", customerList);
        return "customerManagement";
    }
}
