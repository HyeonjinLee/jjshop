package com.jjshop.controller;

import com.jjshop.model.Cart;
import com.jjshop.model.Customer;
import com.jjshop.model.CustomerOrder;
import com.jjshop.service.CartService;
import com.jjshop.service.CustomerOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by jin on 2016-06-29.
 */

@Controller
public class OrderController {

    @Autowired
    private CartService cartService;

    @Autowired
    CustomerOrderService customerOrderService;

    @RequestMapping("/order/{cartId}")
    public String createOrder(@PathVariable ("cartId") int cartId) {
        CustomerOrder cusOrder = new CustomerOrder();
        Cart cart = cartService.getCartById(cartId);
        cusOrder.setCart(cart);

        Customer customer = cart.getCustomer();
        cusOrder.setCustomer(customer);
        cusOrder.setBillingAddress(customer.getBillingAddress());
        cusOrder.setShippingAddress(customer.getShippingAddress());

        customerOrderService.addCustomerOrder(cusOrder);

        return "redirect:/checkout?cartId="+cartId;
    }
}
