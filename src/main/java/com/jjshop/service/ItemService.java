package com.jjshop.service;

import com.jjshop.model.Item;

import java.util.List;

/**
 * Created by jin on 2016-06-27.
 */
public interface ItemService {

    List<Item> getItemList();

    Item getItemById(int itemId);

    void addItem(Item item);

    void editItem(Item item);

    void deleteItem(Item item);

}
