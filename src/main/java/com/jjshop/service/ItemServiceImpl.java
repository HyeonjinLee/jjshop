package com.jjshop.service;

import com.jjshop.dao.ItemDao;
import com.jjshop.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jin on 2016-06-27.
 */

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemDao itemDao;


    @Override
    public Item getItemById(int itemId) {
        return itemDao.getItemById(itemId);
    }

    @Override
    public List<Item> getItemList() {
        return itemDao.getAllItems();
    }

    @Override
    public void addItem(Item item) {
        itemDao.addItem(item);
    }

    @Override
    public void editItem(Item item) {
        itemDao.editItem(item);
    }

    @Override
    public void deleteItem(Item item) {
        itemDao.deleteItem(item);
    }
}
