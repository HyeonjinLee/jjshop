package com.jjshop.service;

import com.jjshop.model.Cart;
import com.jjshop.model.CartItem;

/**
 * Created by jin on 2016-06-27.
 */
public interface CartItemService {

    void addCartItem(CartItem cartItem);

    void removeCartItem(CartItem cartItem);

    void removeAllCartItems(Cart cart);

    CartItem getCartItemByItemId(int itemId);

}
