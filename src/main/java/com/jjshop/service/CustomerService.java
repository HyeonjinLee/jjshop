package com.jjshop.service;

import com.jjshop.model.Customer;

import java.util.List;

/**
 * Created by jin on 2016-06-27.
 */
public interface CustomerService {

    void addCustomer (Customer customer);

    Customer getCustomerById (int customerId);

    List<Customer> getAllCustomer();

    Customer getCustomerByUsername (String username);

}
