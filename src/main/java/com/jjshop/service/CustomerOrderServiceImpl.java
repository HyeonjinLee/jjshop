package com.jjshop.service;

import com.jjshop.dao.CustomerOrderDao;
import com.jjshop.model.Cart;
import com.jjshop.model.CartItem;
import com.jjshop.model.CustomerOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jin on 2016-06-29.
 */

@Service
public class CustomerOrderServiceImpl implements CustomerOrderService{

    @Autowired
    private CustomerOrderDao customerOrderDao;

    @Autowired
    private CartService cartService;

    @Override
    public void addCustomerOrder(CustomerOrder cusOrder) {
        customerOrderDao.addCustomerOrder(cusOrder);
    }

    @Override
    public double getCustomerOrderGrandTotal(int cartId) {
        double grandTotal = 0;
        Cart cart = cartService.getCartById(cartId);
        List<CartItem> cartItems = cart.getCartItems();

        for (CartItem item : cartItems){
            grandTotal+= item.getTotalPrice();
        }
        return grandTotal;
    }
}
