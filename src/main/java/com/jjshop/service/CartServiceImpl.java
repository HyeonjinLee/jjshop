package com.jjshop.service;

import com.jjshop.dao.CartDao;
import com.jjshop.model.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by jin on 2016-06-27.
 */

@Service
public class CartServiceImpl implements CartService{

    @Autowired
    private CartDao cartDao;


    @Override
    public Cart getCartById(int cartId) {
        return cartDao.getCartById(cartId);
    }

    @Override
    public void update(Cart cart) {
        cartDao.update(cart);
    }
}
