package com.jjshop.service;

import com.jjshop.model.CustomerOrder;

/**
 * Created by jin on 2016-06-29.
 */
public interface CustomerOrderService {

    void addCustomerOrder(CustomerOrder cusOrder);

    double getCustomerOrderGrandTotal(int cartId);

}
