package com.jjshop.service;

import com.jjshop.model.Cart;

/**
 * Created by jin on 2016-06-27.
 */
public interface CartService {

    Cart getCartById(int cartId);

    void update(Cart cart);

}
