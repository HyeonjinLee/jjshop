package com.jjshop.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by jin on 2016-06-27.
 */

@Entity
@Data
public class Authorities {

    @Id @GeneratedValue
    private int authoritiesId;
    private String username;
    private String authority;

}
