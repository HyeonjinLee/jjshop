package com.jjshop.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jin on 2016-06-23.
 */

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class CartItem implements Serializable{

    private static final long serialVersionUID = -6429420122529777324L;

    @Id
    @GeneratedValue
    private int cartItemId;

    @ManyToOne
    @JoinColumn(name = "cartId")
    @JsonIgnore
    private Cart cart;

    @ManyToOne
    @JoinColumn(name = "itemId" )
    private Item item;

    private int quantity;
    private double totalPrice;



}
