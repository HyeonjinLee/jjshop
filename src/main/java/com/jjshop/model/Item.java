package com.jjshop.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.util.List;

/**
 * Created by jin on 2016-07-07.
 */

@Data
@Entity
public class Item implements Serializable {


    private static final long serialVersionUID = -2346414358974972502L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int itemId;

    @NotEmpty(message = " The name must not be empty !! ")
    private String itemName;
    private String itemCategory;
    private String itemDescription;

    @Min(value = 0, message = "The price must not be less than 0 !!")
    private double itemPrice;
    private String itemCondition;
    private String itemStatus;

    @Min(value = 0, message = "The unit must not be less than 0 !!")
    private int unitStock;
    private String itemManufacturer;
    @Transient
    private MultipartFile itemImage;

    @OneToMany(mappedBy = "item", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnore
    private List<CartItem> cartItemList;
}
