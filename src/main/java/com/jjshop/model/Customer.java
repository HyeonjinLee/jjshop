package com.jjshop.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jin on 2016-06-27.
 */

@Entity
@Getter
@Setter
public class Customer implements Serializable {

    private static final long serialVersionUID = -5662745784564588979L;

    @Id @GeneratedValue
    private int customerId;

    @NotEmpty(message = "The name must not be empty!! ")
    private String customerName;

    @NotEmpty(message = "The email must not be empty!!")
    private String customerEmail;
    private String customerPhone;

    @NotEmpty(message = "The username must not be empty!!")
    private String username;

    @NotEmpty(message = "The password must not be empty!!")
    private String password;

    private boolean enabled;

    @OneToOne
    @JoinColumn(name="billingAddressId")
    private BillingAddress billingAddress;

    @OneToOne
    @JoinColumn(name="shippingAddressId")
    private ShippingAddress shippingAddress;

    @OneToOne
    @JoinColumn(name = "cartId")
    @JsonIgnore
    private Cart cart;
}
