package com.jjshop.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jin on 2016-06-27.
 */

@Data
@Entity
public class CustomerOrder implements Serializable{


    private static final long serialVersionUID = -1964351454484705151L;

    @Id @GeneratedValue
    private int customerOrderId;

    @OneToOne
    @JoinColumn(name = "cartId")
    private Cart cart;

    @OneToOne
    @JoinColumn(name = "customerId")
    private Customer customer;

    @OneToOne
    @JoinColumn(name = "billingAddressId")
    private BillingAddress billingAddress;

    @OneToOne
    @JoinColumn(name = "shippingAddressId")
    private ShippingAddress shippingAddress;

}
