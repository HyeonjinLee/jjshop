<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@include file="/WEB-INF/views/template/header.jsp" %>


<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1> Register Customer </h1>
            <p class="lead"> Fill in all info of register !!! </p>
        </div>

        <form:form action="${pageContext.request.contextPath}/register?${_csrf.parameterName}=${_csrf.token}"
                   method="post"
                   commandName="customer">

            <%--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>--%>
        <h3> Customer Info </h3>

        <div class="form-group">
            <label for="customerName">Name</label><form:errors path="customerName" cssStyle="color: #ff0b40" />
            <form:input path="customerName" id="customerName" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="email"> Email</label><span style="color: #ff0b40" >${emailMsg}</span>
            <form:errors path="customerEmail" cssStyle="color: #ff0b40" />
            <form:input path="customerEmail" id="email" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="phone"> Phone </label>
            <form:input path="customerPhone" id="phone" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="username"> Username </label><span style="color: #ff0b40" >${usernameMsg}</span>
            <form:errors path="username" cssStyle="color: #ff0b40" />
            <form:input path="username" id="username" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="password"> Password</label><form:errors path="password" cssStyle="color: #ff0b40" />
            <form:password path="password" id="password" class="form-Control"/>
        </div>

        <h3> Billing Address </h3>

        <div class="form-group">
            <label for="billingStreet"> Street Name </label>
            <form:input path="billingAddress.streetName" id="billingStreet" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="billingApartmentNumber"> Apartment Number </label>
            <form:input path="billingAddress.apartmentNumber" id="billingApartmentNumber" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="billingCity"> City </label>
            <form:input path="billingAddress.city" id="billingCity" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="billingState"> State </label>
            <form:input path="billingAddress.state" id="billingState" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="billingCountry"> Country </label>
            <form:input path="billingAddress.country" id="billingCountry" class="form-Control"/>
        </div>


        <div class="form-group">
            <label for="billingZipCode"> Zipcode </label>
            <form:input path="billingAddress.zipCode" id="billingZipCode" class="form-Control"/>
        </div>


        <h3> Shipping Address </h3>

        <div class="form-group">
            <label for="shippingStreet"> Street Name </label>
            <form:input path="shippingAddress.streetName" id="shippingStreet" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="shippingApartmentNumber"> Apartment Number </label>
            <form:input path="shippingAddress.apartmentNumber" id="shippingApartmentNumber" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="shippingCity"> City </label>
            <form:input path="shippingAddress.city" id="shippingCity" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="shippingState"> State </label>
            <form:input path="shippingAddress.state" id="shippingState" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="shippingCountry"> Country </label>
            <form:input path="shippingAddress.country" id="shippingCountry" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="shippingZipCode"> Zipcode </label>
            <form:input path="shippingAddress.zipCode" id="shippingZipCode" class="form-Control"/>
        </div>

        <br><br>

        <input type="submit" value="submit" class="btn btn-primary"/>
        <a href="<c:url value="/" />" class="btn btn-danger">Cancel</a>

        </form:form>

        <%@include file="/WEB-INF/views/template/footer.jsp" %>
