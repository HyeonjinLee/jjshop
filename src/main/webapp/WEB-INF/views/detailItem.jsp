<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@include file="/WEB-INF/views/template/header.jsp" %>
<script src="<c:url value="/resources/js/controller.js" />" ></script>

<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1>Item Detail </h1>
            <p class="lead"> Check the item detail information !!!</p>
        </div>

        <div class="container" ng-app="cartApp">
            <div class="row">
                <div class="col-md-5">
                    <img src="<c:url value="/resources/images/${item.itemId}.jpg" /> " alt="image" style="width:100%"/>
                </div>
                <div class="col-md-5">
                    <h3>${item.itemName}</h3>
                    <p>${item.itemDescription}</p>
                    <p> <strong>Menufacture</strong> : ${item.itemManufacturer}</p>
                    <p> <strong>Category</strong> : ${item.itemCategory} </p>
                    <p> <strong>Condition</strong> : ${item.itemCondition}</p>
                    <h4> $${item.itemPrice} CA </h4>
                    <br><br>

                    <sec:authorize access="hasRole('ROLE_USER')" var="isUser" />
                    <sec:authorize access="hasRole('ROLE_ADMIN')" var="isAdmin" />

                    <c:set var="role" scope="page" value="${param.role}" />
                    <c:set var="url" scope="page" value="/item/itemList" />
                    <c:if test="${isAdmin}">
                        <c:set var="url" scope="page" value="/admin/itemInventory" />
                    </c:if>
                    <%--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>--%>
                    <p ng-controller="cartCtrl">
                    <p ng-controller="cartCtrl">
                        <a href="<c:url value="${url}" /> " class="btn btn-info">Back</a>
                        <a href="#" class="btn btn-warning btn-large" ng-click="addToCart('${item.itemId}')">
                            <span class="glyphicon glyphicon-shopping-cart"></span> Order
                        </a>
                        <a href="<spring:url value="/customer/cart/" />" class="btn btn-primary">
                            <span class="glyphicon glyphicon-hand-right"></span> View Cart
                        </a>

                    </p>
                </div>
            </div>
        </div>


        <%@include file="/WEB-INF/views/template/footer.jsp" %>
