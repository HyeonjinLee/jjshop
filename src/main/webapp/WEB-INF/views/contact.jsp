<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@include file="/WEB-INF/views/template/header.jsp" %>


<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1> Contact Form</h1>
            <p class="lead"> Send your comments through this form and we will get back to you. </p>
        </div>

        <form class="form-horizontal" role="form" action="javascript:alert('Thank you for contact us (:');">

            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Name </label>
                <div class="col-sm-10">
                    <input id="name" class="form-Control" placeholder="First & Last Name"/>
                </div>
            </div>


            <div class="form-group">
                <label for="email" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" id="email" name="email" placeholder="example@domain.com">
                </div>
            </div>

            <div class="form-group">
                <label for="phone" class="col-sm-2 control-label">Phone Number</label>
                <div class="col-sm-10">
                    <input id="phone" class="form-Control"/>
                </div>
            </div>


            <div class="form-group">
                <label for="message" class="col-sm-2 control-label">Message</label>
                <div class="col-sm-10">
                    <textarea class="form-control" rows="4" id="message"></textarea>
                </div>
            </div>

            <br><br>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <input type="submit" value="Send Message" class="btn btn-primary">
                </div>
            </div>
        </form>

        <%@include file="/WEB-INF/views/template/footer.jsp" %>
