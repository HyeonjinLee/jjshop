<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@include file="/WEB-INF/views/template/header.jsp"%>


<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1>Item Inventory</h1>
            <p class="lead"> Check the item inventory !!!</p>
        </div>

        <table class="table table-striped table-hover">
            <thead>
            <tr class="bg-success">
                <th>Item photo</th>
                <th>Item Name</th>
                <th>Category</th>
                <th>Condition</th>
                <th>Price</th>
                <th></th>
            </tr>
            </thead>
            <c:forEach items="${items}" var="item">
                <tr>
                    <td><img src="<c:url value="/resources/images/${item.itemId}.jpg" /> " alt="image" style="width:100%"/></td>
                    <td>${item.itemName}</td>
                    <td>${item.itemCategory}</td>
                    <td>${item.itemCondition}</td>
                    <td>$${item.itemPrice} CA</td>
                    <td><a href="<spring:url value="/item/detailItem/${item.itemId}" /> "><span class="glyphicon glyphicon-info-sign"></span></a>
                        <a href="<spring:url value="/admin/item/deleteItem/${item.itemId}" /> "><span class="glyphicon glyphicon-remove-sign"></span></a>
                        <a href="<spring:url value="/admin/item/editItem/${item.itemId}" /> "><span class="glyphicon glyphicon-pencil"></span></a></td>
                </tr>
            </c:forEach>
        </table>

        <a class="btn btn-primary" href="<spring:url value="/admin/item/addItem"/>" > Add Item</a>

        <%@include file="/WEB-INF/views/template/footer.jsp" %>



