<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@include file="/WEB-INF/views/template/header.jsp" %>


<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1>Add Item </h1>
            <p class="lead"> Fill in all info of item !!! </p>
        </div>

        <form:form action="${pageContext.request.contextPath}/admin/item/addItem?${_csrf.parameterName}=${_csrf.token}" method="post"
                   commandName="item" enctype="multipart/form-data">
            <%--<sec:csrfInput />--%>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <div class="form-group">
            <label for="itemName">Name</label> <form:errors path="itemName" cssStyle="color: #ff1b32;"/>
            <form:input path="itemName" id="itemName" class="form-Control"/>
        </div>
        <div class="form-group">
            <label class="category"> Category </label>
            <label class="checkbox-inline"><form:radiobutton path="itemCategory" id="category" value="cosmetic"/>
                Cosmetic</label>
            <label class="checkbox-inline"><form:radiobutton path="itemCategory" id="category" value="cloth"/> Cloth </label>
            <label class="checkbox-inline"><form:radiobutton path="itemCategory" id="category" value="etc"/>
                Other </label>
        </div>

        <div class="form-group">
            <label for="itemDescription">Description</label>
            <form:textarea path="itemDescription" id="itemDescription" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="itemPrice">Price</label> <form:errors path="itemPrice" cssStyle="color: #ff1b32;"/>
            <form:input path="itemPrice" id="itemPrice" class="form-Control"/>
        </div>

        <div class="form-group">
            <label class="condition"> Condition </label>
            <label class="checkbox-inline"><form:radiobutton path="itemCondition" id="itemCondition" value="new"/>
                New</label>
            <label class="checkbox-inline"><form:radiobutton path="itemCondition" id="itemCondition" value="used"/>
                Used</label>
        </div>

        <div class="form-group">
            <label class="status"> Status</label>s
            <label class="checkbox-inline"><form:radiobutton path="itemStatus" id="itemStatus" value="active"/>
                Active</label>
            <label class="checkbox-inline"><form:radiobutton path="itemStatus" id="itemStatus" value="inactive"/>
                Inactive</label>
        </div>

        <div class="form-group">
            <label for="unitStock">Unit Stock </label> <form:errors path="unitStock" cssStyle="color: #ff1b32;"/>
            <form:input path="unitStock" id="unitStock" class="form-Contrl"/>
        </div>

        <div class="form-group">
            <label for="itemManufacturer"> Manufacturer</label>
            <form:input path="itemManufacturer" id="itemManufacturer" class="form-Control"/>
        </div>

        <div class="form-group">
            <label class="control-label" for="itemImage"> Upload Picture</label>
            <form:input path="itemImage" id="itemImage" type="file" class="form:input-large"/>
        </div>

        <br><br>

        <input type="submit" value="submit" class="btn btn-primary"/>
        <a href="<c:url value="/admin/itemInventory" />" class="btn btn-danger">Cancel</a>

        </form:form>

        <%@include file="/WEB-INF/views/template/footer.jsp" %>
