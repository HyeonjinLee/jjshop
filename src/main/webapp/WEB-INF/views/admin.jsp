<%@include file="/WEB-INF/views/template/header.jsp"%>


<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1>Administrator Page</h1>
            <p class="lead"> Admin Management Page </p>
        </div>

        <%--<c:if test="${pageContext.request.userPrincipal.name != null}">--%>
            <%--<h2>--%>
                <%--Welcome: ${pageContext.request.userPrincipal.name} |--%>
                        <%--<a href="<c:url value="/logout" />">Logout</a>--%>
            <%--</h2>--%>
        <%--</c:if>--%>

        <p> View the item inventory </p>

        <h3><a href="<c:url value="/admin/itemInventory" />" >Items Inventory</a></h3>

        <br>
        <hr>

        <p> Check out the customer information </p>

        <h3><a href="<c:url value="/admin/customer" />" > Customer Management </a></h3>


        <%@include file="/WEB-INF/views/template/footer.jsp" %>



