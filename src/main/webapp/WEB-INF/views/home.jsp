<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@include file="/WEB-INF/views/template/header.jsp"%>

<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img class="first-slide home-image" src="<c:url value="/resources/images/mainPage/main1.jpg "/> " alt="First slide">
            <div class="container">
                <div class="carousel-caption">
                    <h1> Welcome to JJShop </h1>
                    <p> Check lots of brand-new item out here and You'll also find a listing of special offers and promotions. Enjoy your shopping!  </p>
                </div>
            </div>
        </div>
        <div class="item">
            <img class="second-slide home-image" src="<c:url value="/resources/images/mainPage/main2.jpg "/> " alt="Second slide">
            <div class="container">
                <div class="carousel-caption">
                </div>
            </div>
        </div>
        <div class="item">
            <img class="third-slide home-image" src="<c:url value="/resources/images/mainPage/main3.jpg "/> " alt="Third slide">
            <div class="container">
                <div class="carousel-caption">
                </div>
            </div>
        </div>
        <div class="item">
            <img class="fourth-slide home-image" src="<c:url value="/resources/images/mainPage/main4.jpg "/> " alt="Fourth slide">
            <div class="container">
                <div class="carousel-caption">
                </div>
            </div>
        </div>
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div><!-- /.carousel -->


<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->

<div class="container marketing">

    <!-- Three columns of text below the carousel -->
    <div class="row">
        <div class="col-lg-4">
            <img class="img-circle" src="<c:url value="/resources/images/mainPage/home1.png "/> " alt="Generic placeholder image" width="140" height="140">
            <h2>S. Korean power rising in global cosmetics scene</h2>
            <p> Display booths bustling with multinational businesspeople from the beauty industry at an exhibition hall in Seoul on Thursday clearly indicated South Korea has now emerged as a significant market for global cosmetics ingredient companies. In the second exhibition, dubbed "in-cosmetics Korea," officials from some 200 multinational cosmetics ingredient providers were in talks with cosmetics manufacturers from around the world in their booths for possible deals.</p>
            <p><a class="btn btn-default" href="http://news.in-cosmetics.com/2015/06/the-rising-wave-of-korean-beauty/" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
            <img class="img-circle" src="<c:url value="/resources/images/mainPage/home2.png "/> " alt="Generic placeholder image" width="140" height="140">
            <h2>The rising wave of Korean beauty</h2>
            <p>In beauty and personal care industry, it is perhaps foreseeable that Asia Pacific will be the next market to look at. Asia Pacific’s influence is stronger in skin care in particular. The region is predicted to generate over half of the total global skin care revenue by 2019, according to Euromonitor’s estimate. A growth engine is growing middle class and its disposable income in emerging countries in Asia Pacific. More importantly, innovative products in Asia Pacific highly contribute to the sales.  </p>
            <p><a class="btn btn-default" href="http://english.yonhapnews.co.kr/focus/2016/07/07/39/1700000000AEN20160707010300320F.html" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
            <img class="img-circle" src="<c:url value="/resources/images/mainPage/home3.png "/> " alt="Generic placeholder image" width="140" height="140">
            <h2>11 Korean Beauty Products That'll Transform Your Skin</h2>
            <p> Here's a sampling of products representing the best of what's happening in Korea right now. And they're all right here in the United States—no plane ticket necessary. Unless you've somehow banked 100,000 frequent-flier miles on a transpacific airline, you might not be heading to Seoul anytime soon. But we bet you're tempted. </p>
            <p><a class="btn btn-default" href="http://www.allure.com/beauty-products/2015/korean-skin-care-products#slide=1" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->


    <%@include file="/WEB-INF/views/template/footer.jsp" %>
