<%@include file="/WEB-INF/views/template/header.jsp" %>


<div class="container-wrapper">
    <div class="container">
        <section>
            <div class="jumbotron">
                <div class="container">
                    <h1> Checkout Cancelled </h1>


                    <p>Your checkout process is cancelled. You may continue shopping</p>
                </div>
            </div>
        </section>

        <section class="container">
            <p>
                <a href="<c:url value="/item/itemList" /> " class="btn btn-primary"> Item List </a>
            </p>
        </section>
    </div>

</div>

<script src="<c:url value="/resources/js/controller.js" />"></script>
<%@include file="/WEB-INF/views/template/footer.jsp" %>