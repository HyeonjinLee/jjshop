<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@include file="/WEB-INF/views/template/header.jsp" %>


<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1> Customer </h1>

            <p class="lead"> Customer Information </p>
        </div>

        <form:form commandName="order" class="from-horizontal">

        <h3> Customer Info </h3>

        <div class="form-group">
            <label for="customerName">Name</label>
            <form:input path="cart.customer.customerName" id="customerName" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="email"> Email</label>
            <form:input path="cart.customer.customerEmail" id="email" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="phone"> Phone </label>
            <form:input path="cart.customer.customerPhone" id="phone" class="form-Control"/>
        </div>


        <h3> Billing Address </h3>

        <div class="form-group">
            <label for="billingStreet"> Street Name </label>
            <form:input path="cart.customer.billingAddress.streetName" id="billingStreet" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="billingApartmentNumber"> Apartment Number </label>
            <form:input path="cart.customer.billingAddress.apartmentNumber" id="billingApartmentNumber" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="billingCity"> City </label>
            <form:input path="cart.customer.billingAddress.city" id="billingCity" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="billingState"> State </label>
            <form:input path="cart.customer.billingAddress.state" id="billingState" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="billingCountry"> Country </label>
            <form:input path="cart.customer.billingAddress.country" id="billingCountry" class="form-Control"/>
        </div>


        <div class="form-group">
            <label for="billingZipCode"> Zipcode </label>
            <form:input path="cart.customer.billingAddress.zipCode" id="billingZipCode" class="form-Control"/>
        </div>

        <input type="hidden" name="_flowExecutionKey" />

        <br><br>

        <input type="submit" value="Next" class="btn btn-primary" name="_eventId_customerInfoCollected"/>
        <button class="btn btn-danger" name="_eventId_cancel">Cancel</button>

        </form:form>

        <%@include file="/WEB-INF/views/template/footer.jsp" %>
