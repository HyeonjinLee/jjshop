/**
 * Created by jin on 2016-06-23.
 */

var cartApp = angular.module("cartApp", []);

cartApp.controller("cartCtrl", cartCtrl);


cartCtrl.$inject = [
    '$scope',
    '$http'
];

function cartCtrl($scope, $http) {

    $scope.initCartId = function (cartId) {
        $scope.cartId = cartId;
        $scope.refreshCart();
    };

    $scope.refreshCart = function () {
        $http.get('/jjshop/rest/cart/' + $scope.cartId).success(function (data) {
            $scope.cart = data;
            $scope.calGrandTotal();
        })
    };


    $scope.addToCart = function (itemId) {
        $http.put('/jjshop/rest/cart/add/' + itemId).then(function (response) {
            alert("Item successfully added to the cart!");
        }, function (response) {
            console.log(" Fail to add item to cart!!! ");
        });
    };

    $scope.removeFromCart = function (itemId) {

        $http({
            method: 'PUT',
               url: '/jjshop/rest/cart/remove/'+itemId
        }).then(function(response){
            $scope.refreshCart();
        }, function (response) {
            console.log(" Fail to remove item from cart!!! ");
        });
    };

    $scope.clearCart = function () {
        var id = $scope.cartId
        $http.delete('/jjshop/rest/cart/'+id).then(function(response){
            $scope.refreshCart();
        }, function (response) {
            console.log(" Fail to delete cart!!! ");
        });
    };

    $scope.calGrandTotal = function () {
        $scope.grandTotal = 0;

        for (var i=0; i<$scope.cart.cartItems.length; i++) {
            $scope.grandTotal+= $scope.cart.cartItems[i].totalPrice;
        }
    }

}

// cartApp.factory('csrfTokenInterceptor', function () {
//     var csrfToken = null;
//     return {
//         response: function (response) {
//             if (response.headers('X-CSRF-Token')) {
//                 csrfToken = response.headers('X-CSRF-Token');
//             }
//             return response;
//         },
//         request: function (config) {
//             if (config.method == 'PUT' && csrfToken) {
//                 config.headers['X-CSRF-Token'] = csrfToken;
//             }
//             return config;
//         }
//     }
// });
//
// cartApp.config(function($httpProvider) {
//
//     $httpProvider.interceptors.push('csrfTokenInterceptor');
//
// }

